import numpy as np
import sys
import rasterio


#def read_header(filein):
#    with open(filein, 'r') as f:
#        Header = ''
#        for i in range(6):
#            header = f.readline()
#            Header = Header + header

    # nodata = int(header.split(' ')[-1])
#    return Header

def read_header(input_file):

    raster = rasterio.open(input_file)
    if raster is None:
        print('Unable to open {}'.format(input_file))
        sys.exit(1)


    header = {}
    header['ncols'] = raster.width
    header['nrows'] = raster.height
    header['xllcorner'] = (raster.transform * (0, 0))[0]
    header['yllcorner'] = (raster.transform * (0, raster.height))[1]
    header['cellsize'] = raster.transform[0]
    header['noDataValue'] = raster.nodata
    return header


def read_raster(input_file):

    header = read_header(input_file)
    raster = rasterio.open(input_file)
    my_array = raster.read(1)

    return my_array, header



if __name__ == "__main__":
    #filein = "/home/damboise/Documents/text_replace/test_dhm_release_s_.asc"
    #filein = "/home/damboise/Documents/text_replace/40m_slope_45obs_hemi_rel.asc"
    filein = "/home/P/Projekte/18130-GreenRisk4Alps/Simulation/PAR5_Wipptal_ITA/resultdebris/class3/elhc3.asc"

    fileout = "/home/P/Projekte/18130-GreenRisk4Alps/Simulation/PAR5_Wipptal_ITA/init/release_class_3.asc"

    raster, header = read_raster(filein)
    # nodata = float(int(header.split(' ')[-1]))
    #raster = np.loadtxt(filein,  skiprows=6)
    # change the raster
    # raster[raster == nodata] = -9999
    #raster[raster >= 0.8] = int(5)
    #quant = [0, 0.25, 0.5, 0.75, 1]
    #raster[raster < 0] = 
    #raster_quant = np.quantile(raster[raster >= 0], quant)
    #raster_class3 = raster
    raster[raster > 9000] = -9999
    raster_trans = rasterio.open(filein)
    # save file
    
    #np.savetxt("/home/P/Projekte/18130-GreenRisk4Alps/Simulation/PAR5_Wipptal_ITA/init/release_class_3.asc", raster, header=header)
    new_dataset = rasterio.open(fileout, 'w', driver='GTiff', height=raster.shape[0], width=raster.shape[1], count=1, dtype=raster.dtype, crs='+proj=latlong', transform=raster_trans.transform)
    new_dataset.write(raster, 1)
    new_dataset.close()
