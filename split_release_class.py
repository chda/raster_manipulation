import numpy as np
import sys
import pandas as pd

def read_header(filein):
    with open(filein, 'r') as f:
        Header = ''
        for i in range(6):
            header = f.readline()
            Header = Header + header
            if i == 0:
                no_cols = int(str.split(header)[1])
            if i == 1:
                no_rows = int(str.split(header)[1])
            elif i == 5:
                no_data = str.split(header)[1]
    return Header, no_cols, no_rows, no_data
    

def line_builder(raster_changed, size_class, no_data):
    #raster_changed[raster > size_class] = int(3)
    raster_changed[raster < size_class] = int(-9999)
    #raster_changed[raster_changed != size_class] = int(0)
    #raster_changed[raster_changed != size_class] = no_data # if cell in raster not == size class set to no data value

    return raster_changed

if __name__ == "__main__":
    #filein = sys.argv[1]
    filein = "/home/P/Projekte/18130-GreenRisk4Alps/Simulation/PAR2_KranjskaGora_SLO/infra/buildings/asci/buildings_int16.asc"
    print (filein)
    header , no_cols, no_rows, no_data_value = read_header(filein)
    print(header)

    #for i in range(1,2,1): # loop over release class
    #print("Class",i, "started")
    raster = pd.read_csv(filein, delim_whitespace = True , header=5, names=list(range(no_cols)), na_values=no_data_value, nrows= no_rows) # call date in data frame because it can handel odd shaped rasters
    print("Raster loaded")
    #raster = np.loadtxt(filein, delimiter=' ', skiprows=6)
    raster_np = raster.to_numpy() # change data frame to numpy array
    print("Raster to numpy")
    ras = line_builder(raster_np, 0, no_data_value) # change raster so only one class is seen
    print("Raster changed")
    np.savetxt('/home/P/Projekte/18130-GreenRisk4Alps/Simulation/PAR2_KranjskaGora_SLO/infra/buildings/asci/infra_class_3.asc', ras, fmt="%d", header=header, comments='') # save raster with header
    print("Raster saved")
        
    
